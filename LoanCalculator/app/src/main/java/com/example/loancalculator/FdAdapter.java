package com.example.loancalculator;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.math.BigDecimal;
import java.util.ArrayList;

public class FdAdapter extends RecyclerView.Adapter<FdAdapter.ViewHolder> {

    private ArrayList<FdItemList> mFdItemList;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mIndex;
        public TextView mMaturityAmount;
        public TextView mTotalAmount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mIndex = itemView.findViewById(R.id.index);
            mMaturityAmount = itemView.findViewById(R.id.maturityAmount);
            mTotalAmount = itemView.findViewById(R.id.totalAmount);
        }
    }

    public FdAdapter(ArrayList<FdItemList> fdItemLists) {
        mFdItemList = fdItemLists;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fd_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        FdItemList currentItem = mFdItemList.get(position);
        String stringMaturity = currentItem.getMaturityAmount();
        String stringTotalAmount = currentItem.getTotalAmount();
        holder.mIndex.setText(String.valueOf(currentItem.getIndex()));
        holder.mMaturityAmount.setText(String.format("%.2f", new BigDecimal(stringMaturity)));
        holder.mTotalAmount.setText(String.format("%.2f", new BigDecimal(stringTotalAmount)));
    }

    @Override
    public int getItemCount() {

        return mFdItemList.size();
    }
}
