package com.example.loancalculator;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.math.BigDecimal;

public class CarLoanCalculatorFragment extends Fragment {
    private static final String TAG = "car_loan_calculator";

    public EditText mPurchasePrice, mDownPayment, mInterestRate, mLoanDuration;
    public Button mButton;
    public TextView mTotalMonthlyPayment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.car_loan_cal, container, false);

        mPurchasePrice = v.findViewById(R.id.ePurchasePrice);
        mDownPayment = v.findViewById(R.id.eDownPayment);
        mInterestRate = v.findViewById(R.id.eInterestRate);
        mLoanDuration = v.findViewById(R.id.eLoanDuration);
        mTotalMonthlyPayment = v.findViewById(R.id.ttlpaymentamount);
        mButton = (Button) v.findViewById(R.id.calloan);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Click calculate car button.");
                if (!isEmpty(mPurchasePrice) && !isEmpty(mDownPayment) && !isEmpty(mInterestRate) && !isEmpty(mLoanDuration)) {
                    calculateLoan();
                }

            }
        });
    }

    private boolean isEmpty(EditText etText) {
        Log.d(TAG, "isEmpty: " + etText.getText());
        if (etText.getText().toString().trim().length() > 0)
            return false;

        return true;
    }

    public void calculateLoan() {

        double loanAmount = Double.parseDouble(mPurchasePrice.getText().toString()) - Double.parseDouble(mDownPayment.getText().toString());
        double rateAmount = (Double.parseDouble(mInterestRate.getText().toString()) / 100);
        double totalRateAmount = rateAmount * (Double.parseDouble(mLoanDuration.getText().toString())) * loanAmount;
        double monthlyRepayment = (totalRateAmount + loanAmount) / (Double.parseDouble(mLoanDuration.getText().toString()) * 12);

        Log.d(TAG, "calculateLoan: " + monthlyRepayment);
        Log.d(TAG, "calculateLoan: " + rateAmount);
        Log.d(TAG, "calculateLoan: " + loanAmount);
        Log.d(TAG, "calculateLoan: " + totalRateAmount);


        String stringTotalMonthlyPayment = String.valueOf(monthlyRepayment);

        mTotalMonthlyPayment.setText(String.format("%.2f", new BigDecimal(stringTotalMonthlyPayment)));

    }
}
