package com.example.loancalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private Button mfdcal, mloancal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mfdcal = findViewById(R.id.fdcal);
        mloancal = findViewById(R.id.loancal);

        mfdcal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Clicked Calculated FD.");
                Intent intent = new Intent(MainActivity.this, FDCalculatorActivity.class);
                startActivity(intent);
            }
        });

        mloancal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Clicked Calculated Loan.");
                Intent intent = new Intent(MainActivity.this, LoanCalculatorActivity.class);
                startActivity(intent);
            }
        });


    }



}
