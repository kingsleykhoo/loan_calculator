package com.example.loancalculator;

public class FdItemList {


    private String maturityAmount;
    private String totalAmount;
    private int index;

    public String getMaturityAmount() {
        return maturityAmount;
    }

    public void setMaturityAmount(String maturityAmount) {
        this.maturityAmount = maturityAmount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public FdItemList(String maturityAmount, String totalAmount, int index) {
        this.maturityAmount = maturityAmount;
        this.totalAmount = totalAmount;
        this.index = index;
    }
}
