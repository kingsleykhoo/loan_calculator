package com.example.loancalculator;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toolbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class FDCalculatorActivity extends AppCompatActivity {

    private static final String TAG = "FD_calculator";
    private EditText mDepositAmount, mInterestRate, mMaturityPeriod, mDepositPeriod;
    private TextView mIndex;
    private RecyclerView mrecyclerView;
    private ArrayList<FdItemList> mFdItemList;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Button mbutton;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fd_cal);
        Log.d(TAG, "onCreate: ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("FD Calculator");

        mDepositAmount = findViewById(R.id.eDeposit_Amount);
        mInterestRate = findViewById(R.id.eInterestRate);
        mMaturityPeriod = findViewById(R.id.eMaturity);
        mDepositPeriod = findViewById(R.id.eDepositPeriod);
        mrecyclerView = findViewById(R.id.recycleList);
        mIndex = findViewById(R.id.index);
        mbutton = findViewById(R.id.calfd);

        mbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isEmpty(mDepositAmount) && !isEmpty(mInterestRate) && !isEmpty(mMaturityPeriod) && !isEmpty(mDepositPeriod)) {
                    FdCalculation();
                }
            }
        });
    }


    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;

        return true;
    }


    public void FdCalculation() {
        Log.d(TAG, "FdCalculation: Start calculation.");

        mFdItemList = new ArrayList<>();
        double myEIR = (Double.parseDouble(mInterestRate.getText().toString()) / 100 / 12);
        double myDoubleInterestRate = myEIR * (Double.parseDouble(mMaturityPeriod.getText().toString()));
        int myIntIndex = Integer.parseInt(mDepositPeriod.getText().toString()) / Integer.parseInt(mMaturityPeriod.getText().toString());

        double myDoubleMaturityAmount = myEIR * Double.parseDouble(mDepositAmount.getText().toString());
        double myInitialDepositAmount = Double.parseDouble(mDepositAmount.getText().toString());


        mrecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mFdItemList = new ArrayList<>();

        double myNewTotalAmount = myInitialDepositAmount;
        double myNewDoubleMaturityAmount;


        for (int i = 1; i <= myIntIndex; i++) {
            myNewDoubleMaturityAmount = myEIR * myNewTotalAmount;
            Log.d(TAG, "FdCalculation: " + myNewDoubleMaturityAmount);
            String myNewStringMaturityAMount = String.valueOf(myNewDoubleMaturityAmount);
            myNewTotalAmount = myNewDoubleMaturityAmount + myNewTotalAmount;
            String myNewStringTotalAmount = String.valueOf(myNewTotalAmount);

            mFdItemList.add(new FdItemList(myNewStringMaturityAMount, myNewStringTotalAmount, i));
            Log.d(TAG, "FdCalculation: " + myNewStringTotalAmount);
        }
        Log.d(TAG, "FdCalculation: calculation ended.");

        mLayoutManager = new LinearLayoutManager(this);
        mrecyclerView.setHasFixedSize(true);
        mAdapter = new FdAdapter(mFdItemList);

        mrecyclerView.setLayoutManager(mLayoutManager);
        mrecyclerView.setAdapter(mAdapter);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
