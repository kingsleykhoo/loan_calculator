package com.example.loancalculator;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.math.BigDecimal;


public class PropertyLoanCalculatorFragment extends Fragment {
    private static final String TAG = "Property_loan_calculato";
    public EditText mPurchasePrice, mDownPayment, mInterestRate, mLoanDuration;
    public Button mButton;
    public TextView mTotalMonthlyPayment;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.loan_cal, container, false);
        mPurchasePrice = v.findViewById(R.id.ePurchasePrice);
        mDownPayment = v.findViewById(R.id.eDownPayment);
        mInterestRate = v.findViewById(R.id.eInterestRate);
        mLoanDuration = v.findViewById(R.id.eLoanDuration);
        mTotalMonthlyPayment = v.findViewById(R.id.ttlpaymentamount);
        mButton = (Button)v.findViewById(R.id.calloan);

        return v;


    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Click calculate property button.");
                if(!isEmpty(mPurchasePrice) && !isEmpty(mDownPayment) && !isEmpty(mInterestRate) && !isEmpty(mLoanDuration) ){
                    calculateLoan();
                }

            }
        });
    }

    private boolean isEmpty(EditText etText) {
        Log.d(TAG, "isEmpty: " + etText.getText());
        if (etText.getText().toString().trim().length() > 0)
            return false;

        return true;
    }

    public void calculateLoan(){
        double monthlyRate = Double.parseDouble(mInterestRate.getText().toString()) / 12 / 100;
        double loanAmount = Double.parseDouble(mPurchasePrice.getText().toString()) - Double.parseDouble(mDownPayment.getText().toString());
        double loanDuration = Double.parseDouble(mLoanDuration.getText().toString()) * 12 ;
        double monthlyRepayment = loanAmount*monthlyRate*(Math.pow((1+ monthlyRate) , loanDuration)) / ((Math.pow((1 + monthlyRate),loanDuration) - 1 ));
        Log.d(TAG, "calculateLoan: " + monthlyRepayment);
        Log.d(TAG, "calculateLoan: " + "monthlyRate" + monthlyRate);
        Log.d(TAG, "calculateLoan: " + "loanAmount" + loanAmount);
        Log.d(TAG, "calculateLoan: " + "loanDuration" + loanDuration);
        String stringTotalMonthlyPayment = String.valueOf(monthlyRepayment);

        mTotalMonthlyPayment.setText(String.format("%.2f", new BigDecimal(stringTotalMonthlyPayment)));

    }

}
